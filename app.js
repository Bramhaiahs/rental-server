const http = require("http");
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const adminRoutes = require('./routes/admin');
const shoproutes = require('./routes/shop');
var db = require('./database/database1')
app.use(bodyParser.urlencoded({ extended: false }));
//var mongodb = require('mongodb');
var mongodb = require('mongodb');
//]]

//var MongoClient = require('mongodb').MongoClient;



app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(adminRoutes);
app.use(shoproutes);

app.use((req, res, next) => {
    res.status(400).send('<h1>page not found</h1>');

});
app.listen(3000);