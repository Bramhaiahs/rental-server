const express = require('express');
const path = require('path');
const router = express.Router();

const productscontroller = require('../controllers/product');
const customercontroller = require('../controllers/customerdetails');
router.get('/add-product', productscontroller.getaddproducts);
router.post('/product', productscontroller.postaddproduct);

router.post('/customer', customercontroller.postcustomerdetails);
module.exports = router;
